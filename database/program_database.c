#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <time.h>
#define PORT 8080

int cek_data(char data[], char nama_file[]){
    char path_file[1024];
    sprintf(path_file, "databases/client_database/%s", nama_file);

    FILE *fakun;
    fakun= fopen(path_file,"r");
    if (fakun == NULL) {
        perror("fopen()");
        return EXIT_FAILURE;
    }
    char line[1024];
    while (fgets(line , sizeof(line) , fakun)!= NULL)
    {
        if (strstr(line , data)!= NULL){
            return 1;
        }
    }
    return 0;
}
void add_log(char log[]){
    FILE *isilog;
    isilog= fopen("log.txt","a");
    if (isilog == NULL) 
        perror("fopen()");
    fputs(log, isilog);
    fputs("\n", isilog);
    fflush(isilog);
    fclose(isilog);       
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    char *path="/home/rendi/sisop";
    int root;
    char autentikasi_akun[1024] = {0};
    char username[1024]={0};

    recv(new_socket, &root, sizeof(root), 0);
    if(!root){
        char auth_response[1024] = {0};
        valread = read( new_socket , autentikasi_akun, 1024);
        // printf("auth akun : %s\n", autentikasi_akun);
        int cek = cek_data(autentikasi_akun, "client_account.txt");
        
        int u=0;
        for(int i=0; i<strlen(autentikasi_akun); i++){
            if(autentikasi_akun[i]==',')
                break;
            username[u]=autentikasi_akun[i];
            u++;
        }
        // printf("cek = %d\n", cek);
        if(cek){
            sprintf(auth_response, "Auth berhasil\n");
        }
        else{
            sprintf(auth_response, "Akunmu tidak terdaftar\n");
        }
        send(new_socket, auth_response, sizeof(auth_response), 0);
    }
    if(root)
        strcpy(username, "root");
    

    char database_digunakan[1024];
    database_digunakan[0]='\0';

    while(1){
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        char req[1024] = {0};
        char response[1024] = {0};
        char log[1024]={0};
        // 2021-05-19 02:05:15:jack:SELECT FROM table1
        
        valread = read( new_socket , req, 1024);
        sprintf(log, "%d-%02d-%02d %02d:%02d:%02d:%s:%s", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, username, req);
        add_log(log);
        printf("%s", req);

        char *create_user = strstr(req, "CREATE USER");
        char *create_database = strstr(req, "CREATE DATABASE");
        char *use= strstr(req, "USE");
    

        if (create_user){
            if(root){
                char akun[20] = {0};
                int u=0;
                char password[20] = {0};
                int p=0;
                for(int i=12; i<strlen(req); i++){
                    if(req[i] == 32){
                        while(1){
                            i++;
                            if(req[i] == 32 && req[i-1] == 'Y' && req[i-2] == 'B'){
                                i++;
                                akun[u] = ',';
                                u++;
                                break;
                            }
                        }
                    }
                    akun[u] = req[i];
                    u++;
                }
                char path_file_akun[100];
                sprintf(path_file_akun, "databases/client_database/client_account.txt");
                FILE *fakun;
                fakun= fopen(path_file_akun,"a");
                if (fakun == NULL) {
                    perror("fopen()");
                    return EXIT_FAILURE;
                }
                fputs(akun, fakun);
                fputs("\n", fakun);
                fflush(fakun);
                fclose(fakun);
                sprintf(response, "CREATE USER SUCCESS\n");
            }
            else{
                sprintf(response, "ACCESS DENIED!!!\n");
            }
            send(new_socket, response, sizeof(response), 0);
        }

        if (create_database){
            char nama_database[1024]={0};
            char path_database_baru[1024]={0};
            int val, n=0;
            for(val=strlen(req); val>0; val--){
                if(req[val] == 32)
                    break;
            }
            for(int i=val+1; i<strlen(req)-2; i++){
                nama_database[n]=req[i];
                n++;
            }
            //printf("%s\n", nama_database);
            sprintf(path_database_baru, "databases/%s", nama_database);
            int result = mkdir(path_database_baru, 0777);

            if(!root){
                char access_database[1024]={0};
                char username[1024]={0};
                int u=0;
                for(int i=0; i<strlen(autentikasi_akun); i++){
                    if(autentikasi_akun[i]==',')
                        break;
                    username[u]=autentikasi_akun[i];
                    u++;
                }
                sprintf(access_database, "%s,%s", nama_database, username);
                FILE *file_akses;
                file_akses= fopen("databases/client_database/access_account.txt","a");
                if (file_akses == NULL) {
                    perror("fopen()");
                    return EXIT_FAILURE;
                }
                fputs(access_database, file_akses);
                fputs("\n", file_akses);
                fflush(file_akses);
                fclose(file_akses);
            }
            database_digunakan[0]='\0';
            strcpy(database_digunakan, nama_database);
            sprintf(response, "CREATE DATABASE SUCCESS\n");
            send(new_socket, response, sizeof(response), 0);
        }

        if (use){
            database_digunakan[0]='\0';
            int val, n=0;
            for(val=strlen(req); val>0; val--){
                if(req[val] == 32)
                    break;
            }
            for(int i=val+1; i<strlen(req)-2; i++){
                database_digunakan[n]=req[i];
                n++;
            }
            char akses[1024];
            char username[1024]={0};
                int u=0;
                for(int i=0; i<strlen(autentikasi_akun); i++){
                    if(autentikasi_akun[i]==',')
                        break;
                    username[u]=autentikasi_akun[i];
                    u++;
                }
            sprintf(akses, "%s,%s", database_digunakan, username);
            int cek = cek_data(akses, "access_account.txt");
            if(cek || root){
                sprintf(response, "%s USED\n", database_digunakan);
            }
            else{
                sprintf(response, "ACCESS DENIED!!!\n");
            }

            send(new_socket, response, sizeof(response), 0);

        }   
}
}
